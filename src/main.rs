extern crate mailparse;
extern crate uuid;
extern crate chrono;
extern crate time;
extern crate serde_json;
extern crate email_task;
use std::io;
use std::io::prelude::*;
use std::process::{Command, Stdio};
use mailparse::*;
use email_task::*;

fn main() {
    let mut email = String::new();
    io::stdin()
        .read_to_string(&mut email)
        .expect("Could not read input.");

    let parsed = parse_mail(email.as_bytes()).unwrap();

    match parsed.headers.get_first_value("Subject").unwrap() {
        Some(subject) => {
            match parsed.headers.get_first_value("From").unwrap() {
                Some(from) => {
                    let task = build_task(format!("Email: {} (From: {})", subject, from));
                    let serialized = serde_json::to_string(&task).unwrap();
                    import(serialized)
                }
                None => println!("Could not find a from."),
            }
        }
        None => println!("Could not find a subject."),
    };
}

fn build_task(body: String) -> Task {
    let spec = time::get_time();
    let naive = chrono::naive::datetime::NaiveDateTime::from_timestamp(spec.sec, spec.nsec as u32);

    let date = email_task::Date::from(naive);
    TaskBuilder::default()
        .status(Status::Pending)
        .uuid(uuid::Uuid::new_v4())
        .entry(date)
        .description(body)
        .priority(Priority::High)
        .build()
        .unwrap()
}

fn import(task_string: String) -> () {
    let process = match Command::new("/usr/local/bin/task")
        .arg("import")
        .arg("-")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn() {
        Err(_) => panic!("Couldn't import"),
        Ok(process) => process,
    };

    match process.stdin.unwrap().write_all(task_string.as_bytes()) {
        Err(_) => panic!("Couldn't write to stdin"),
        Ok(_) => {}
    }

    let mut s = String::new();

    match process.stdout.unwrap().read_to_string(&mut s) {
        Err(_) => panic!("Couldn't write from stdout"),
        Ok(_) => {}
    }
}
