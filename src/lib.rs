#[macro_use]
extern crate derive_builder;
#[macro_use]
extern crate serde_derive;

extern crate uuid;
extern crate serde;
extern crate serde_json;
extern crate chrono;


use uuid::*;
use std::fmt::{Display, Formatter, Error as FmtError};
use serde::*;
use chrono::NaiveDateTime;
use std::ops::{Deref, DerefMut};

#[derive(Builder, Clone, Debug, Serialize)]
pub struct Task {
    status: Status,
    uuid: Uuid,
    entry: Date,
    description: String,
    priority: Priority,
}

#[derive(Debug, Clone, PartialEq, Serialize)]
pub enum Priority {
    /// Low prio for a Task
    #[serde(rename = "L")]
    Low,

    /// Medium prio for a Task
    #[serde(rename = "M")]
    Medium,

    /// High prio for a Task
    #[serde(rename = "H")]
    High,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize)]
pub enum Status {
    /// Pening status type
    #[serde(rename = "pending")]
    Pending,

    /// Deleted status type
    #[serde(rename = "deleted")]
    Deleted,

    /// Completed status type
    #[serde(rename = "completed")]
    Completed,

    /// Waiting status type
    #[serde(rename = "waiting")]
    Waiting,

    /// Recurring status type
    #[serde(rename = "recurring")]
    Recurring,
}

impl Display for Status {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError> {
        match self {
            &Status::Pending => write!(fmt, "Pending"),
            &Status::Deleted => write!(fmt, "Deleted"),
            &Status::Completed => write!(fmt, "Completed"),
            &Status::Waiting => write!(fmt, "Waiting"),
            &Status::Recurring => write!(fmt, "Recurring"),
        }
    }
}


#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub struct Date(NaiveDateTime);

impl Deref for Date {
    type Target = NaiveDateTime;

    fn deref(&self) -> &NaiveDateTime {
        &self.0
    }
}

impl DerefMut for Date {
    fn deref_mut(&mut self) -> &mut NaiveDateTime {
        &mut self.0
    }
}

impl From<NaiveDateTime> for Date {
    fn from(ndt: NaiveDateTime) -> Date {
        Date(ndt)
    }
}

/// The date-time parsing template used to parse the date time data exported by taskwarrior.
pub static TASKWARRIOR_DATETIME_TEMPLATE: &'static str = "%Y%m%dT%H%M%SZ";

impl Serialize for Date {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer
    {
        let formatted = self.0.format(TASKWARRIOR_DATETIME_TEMPLATE);
        serializer.serialize_str(&format!("{}", formatted))
    }
}
